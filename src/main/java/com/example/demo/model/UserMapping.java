package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserMapping {
    private int totalRecord;
    private List<UserDTO> userList;


    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    @Builder
    public static class UserDTO {
        private String id;
        private String firstName;
        private String lastName;
        private String email;
        private String dateOfBirth;
    }
}