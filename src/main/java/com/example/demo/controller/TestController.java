package com.example.demo.controller;


import com.example.demo.model.User;
import com.example.demo.model.UserMapping;
import com.example.demo.repository.UserRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1")
public class TestController {
    private final UserRepository userRepository;

    @GetMapping("/todo")
    public UserMapping getData(@RequestParam(name = "method", required = false, defaultValue = "") String method) {
        // get all table User
        List<User> users = userRepository.findAll();
        switch (method) {
            case "1":
                // save 5000 record and return list ~ time handle: 5000 record - 77 ms , with 30000 record handle 1.3 s
                List<User> randomUsers = generateRandomUsers(5000);
                userRepository.saveAll(randomUsers);
                users = userRepository.findAll();
                break;
            case "2":
                // delete batch all with id ~ time handle: 5000 record - 200 ms , with 30000 record hanlde 750 ms
                List<String> ids = users.stream().map(User::getId).filter(Objects::nonNull).collect(Collectors.toList());
                userRepository.deleteAllById(ids);
                users = userRepository.findAll();
                break;
            case "3":
                // delete with id by foreach ~ time handle: 5000 record - 1.2 s , with 30000 record handle 8.32 s
                users.forEach(user -> userRepository.deleteById(user.getId()));
                users = userRepository.findAll();
                break;
            default:
                // only select database ~ time handle: 5000 record - 77 ms , with 30000 record handle 500 ms
                break;
        }
        ObjectMapper objectMapper = new ObjectMapper();
        return UserMapping.builder()
                .totalRecord(users.size())
                // userList example pase object to object using ObjectMapper
                .userList(objectMapper.convertValue(users, new TypeReference<List<UserMapping.UserDTO>>() {
                }))
                .build();
    }

    private List<User> generateRandomUsers(int count) {
        List<User> users = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < count; i++) {
            User user = new User();
            user.setFirstName("User" + i);
            user.setLastName("Lastname" + i);
            user.setEmail("user" + i + "@example.com");
            user.setDateOfBirth(generateRandomDate());
            // You can set other fields as needed

            users.add(user);
        }

        return users;
    }

    public static String generateRandomDate() {
        Random random = new Random();
        int currentYear = LocalDate.now().getYear();

        // Generate a random year between 1970 and the current year
        int year = random.nextInt(currentYear - 1970 + 1) + 1970;

        // Generate a random month between 1 and 12
        int month = random.nextInt(12) + 1;

        // Generate a random day between 1 and the maximum days in the selected month
        int maxDaysInMonth = getMaxDaysInMonth(year, month);
        int day = random.nextInt(maxDaysInMonth) + 1;

        // Format the date components as a string in "YYYY-MM-dd" format
        String formattedDate = String.format("%04d-%02d-%02d", year, month, day);

        return formattedDate;
    }

    public static int getMaxDaysInMonth(int year, int month) {
        // An array of the maximum days in each month (accounting for leap years)
        int[] maxDaysPerMonth = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        // Adjust for February in leap years
        if (isLeapYear(year) && month == 2) {
            return 29;
        }

        return maxDaysPerMonth[month];
    }

    public static boolean isLeapYear(int year) {
        return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
    }
}
