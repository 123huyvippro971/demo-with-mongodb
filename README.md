Hướng dẫn run project
1. Load pom
2. Vào file application-os.yml sửa uri: mongodb://localhost:27017/admin.mydbtest -> link mongo tương ứng
3. Vào file User nằm trong com.example.demo.model để sửa tên bảng (collection) muốn kết nối vào
   3.1 @Document(collection = "mydbtest") (Dòng sửa mydbtest thành collection tương ứng)
3. Chạy debugs project là xong

#Note
1. Code nằm trong file TestController
2. Các link gọi tương ứng (Gọi postman với phương thức GET hoặc ném thẳng lên thanh trình duyệt)
   2.1 Hàm lấy danh sách dữ liệu: localhost:8081/api/v1/todo
   2.2 Hàm insert và lấy ra danh sách dữ liệu: localhost:8081/api/v1/todo?method=1
   2.3 Hàm lấy danh sách dữ liệu và delete batch: localhost:8081/api/v1/todo
   2.4 Hàm lấy danh sách dữ liệu và delete theo foreach: localhost:8081/api/v1/todo
   
